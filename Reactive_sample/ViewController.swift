//
//  ViewController.swift
//  Reactive_sample
//
//  Created by 水野祥子 on 2017/06/05.
//  Copyright © 2017年 sachiko. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import Result

//https://github.com/JQHee/SwiftReactivecocoa (参考)
//http://qiita.com/rnsm504/items/1d785f58820df48b60b2(参考)
class ViewController: UIViewController, UIWebViewDelegate , UITextFieldDelegate{
    
    @IBOutlet weak var mylabel: UILabel!
    
    @IBOutlet weak var textfield: UITextField!
    
    @IBOutlet weak var webView: UIWebView!
    
    let (update, updateObserver) = Signal<String, NoError>.pipe()
    
    let (updateTextSignal, updateTextObserver) = Signal<String, NSError>.pipe()
    
    
    let (signal, observer) = Signal<Int, NSError>.pipe()
    let (sachiko, kameko) = Signal<String, NSError>.pipe()
    let (hello, hi) = Signal<String, NSError>.pipe()
    let property = MutableProperty(1)
    var otherProperty = MutableProperty(0)
    
    var textsample = MutableProperty<String>("")
    var textsample2 = MutableProperty("")
    
    var cocoaAction : CocoaAction<Any>!

    let producer = SignalProducer<Int, NSError> { observer, _ in
        print("🐢")
    }
    
    var vm : ViewModel = ViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        textfield.delegate = self
        
        //vmで送った通知がここにくるそしてurlを表示
        vm.action!.values.observeValues { value in
            print(value.absoluteString!)
            self.webView.loadRequest(NSURLRequest(url: value as URL) as URLRequest)
        }
        
        
        //キーボードリターンキーで発動。aにself.textfield.textを入れる
        let a = textfield.reactive.controlEvents(.editingDidEndOnExit)
            .map { value -> String in
                self.textfield.text!
        }
        vm.urlText <~ a
        
        cocoaAction = CocoaAction(vm.action, input: self.textfield.text!)
        //テキストフィールドのエンターキーが押された時のアクションの宣言。
        textfield.addTarget(cocoaAction, action: CocoaAction<Any>.selector, for: .editingDidEndOnExit)

        
        property.producer.startWithValues {
            print("Property received \($0)")
        }
    
        property <~ otherProperty
        
        
        signal.observeResult { result in
            switch result {
            case let .success(value):
                print("value: \(value)")
            case let .failure(error):
                print("error: \(error)")
            }
        }
        
        sachiko.observeResult { result in
            switch result {
            case let .success(value):
                print("value: \(value)")
            case let .failure(error):
                print("error: \(error)")
            }
        }
        kameko.send(value: "頑張れお勉強！！")
        
        
        hello.observeResult { (value) in
            print("こんにちは \(value)")
        }
        
        hi.send(value: "signalA")
        
        
        let center = NotificationCenter.default.reactive
        
        center.notifications(forName: .UIKeyboardWillShow).observe { notification in
            print("Keyboard will show!🐥")
            
        }
        
    
        NotificationCenter.default.reactive.notifications(forName: Notification.Name(rawValue: "home")).observeValues { (value) in
            print(value.object ?? "")
            print("🐥🐥")
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "home"), object: nil)
        
        NotificationCenter.default.reactive.notifications(forName: Notification.Name(rawValue: "UIKeyboardWillShowNotification" ), object: nil).observeValues { (value) in
            print("🍑")
        }
        
                

        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func myBottun(_ sender: Any) {
        mylabel.text = "wa!!"
        observer.send(value: 1)
        otherProperty = MutableProperty(3)
        property <~ otherProperty
        
        producer.start()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



