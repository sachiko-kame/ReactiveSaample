//
//  ViewModel.swift
//  Reactive_sample
//
//  Created by 水野祥子 on 2017/07/30.
//  Copyright © 2017年 sachiko. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import Result


class ViewModel: NSObject {
    
    var urlText : MutableProperty<String> = MutableProperty("")
    var action : Action<String, NSURL, NSError>!
    var validation: MutableProperty<Bool>!
    
    override init() {
        super.init()
        
        validation = MutableProperty(false)
        
        //テキストフィールドのエンターキーによって通知を受け取りクロージャー内で処理。valueには入力したurl
        urlText.producer.startWithValues { value in
            if(value.characters.count > 5){
                print("true")
                self.validation <~ MutableProperty(true)
            }
        }
        
        //validationがtrueで実行
        action = Action(enabledIf: validation) { value in
            return SignalProducer { (observer, disposable) in
                print("action: " + self.urlText.value)
                observer.send(value: NSURL(string: self.urlText.value)!)
                observer.sendCompleted()
            }
        }
        
    }
    
    
}
